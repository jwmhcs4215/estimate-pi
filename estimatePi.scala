import org.apache.spark.{SparkConf, SparkContext}
import org.apache.spark.SparkContext._

val sc = new SparkContext("local", "Pi estimation", new SparkConf())
val NUM_SAMPLES = 5000

val count = sc.parallelize(1 to NUM_SAMPLES).filter { _ =>
  val x = math.random
  val y = math.random
  x*x + y*y < 1
}.count()
println(s"Approximate value of Pi = ${4.0 * count / NUM_SAMPLES}")

sc.stop()
System.exit(0)