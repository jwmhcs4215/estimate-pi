# Estimate Pi

Understood SparkContext, filter,  parallelize, count.

filter is a transformation operation in Spark hence it is lazily evaluated
It is a narrow operation as it is not shuffling data from one partition to multiple partitions
filter accepts predicate as an argument and will filter the elements from source RDD which are not satisfied by predicate function

Parallelized collections are created by calling SparkContext’s parallelize method on an existing collection in your driver program (a Scala Seq). The elements of the collection are copied to form a distributed dataset that can be operated on in parallel.
Once created, the distributed dataset (distData) can be operated on in parallel. 
One important parameter for parallel collections is the number of partitions to cut the dataset into. Spark will run one task for each partition of the cluster. Typically you want 2-4 partitions for each CPU in your cluster. Normally, Spark tries to set the number of partitions automatically based on your cluster. However, you can also set it manually by passing it as a second parameter to parallelize (e.g. sc.parallelize(data, 10))
